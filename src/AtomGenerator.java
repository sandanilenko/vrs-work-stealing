import java.util.Random;
import java.util.concurrent.RecursiveAction;

/**
 * Created by sandan on 28.05.16.
 */
class AtomGenerator extends RecursiveAction {
    private Atom atom;
    static private Random random = new Random();

    AtomGenerator(Atom atom) {
        this.atom = atom;
    }

    static private Boolean isGenerate() {
        return random.nextDouble() < 0.1;
    }

    protected void compute() {
        while (Atom.countInstances < 50000) {
            if (isGenerate()) {
                Atom newAtom = new Atom();
                AtomGenerator task = new AtomGenerator(newAtom);
                task.fork(); // запускаем асинхронно
            }
        }
    }

}