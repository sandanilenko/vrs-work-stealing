import java.util.concurrent.ForkJoinPool;

public class Main {

    public static void main(String[] args) {
        Atom atom = new Atom();
        new ForkJoinPool().invoke(new AtomGenerator(atom));
    }
}
